# MovieNode

It is a microservice with two endpoints, the first one brings any match with respect to a title and the second one allows to modify the "Plot" of each movie to replace certain words.

## Installation

Use the node [npm](https://github.com/nodejs/nodejs.dev) package manager to install the project dependencies.

```bash
npm install
```

## Usage

```python
# start project locally
npm run dev

# start project in production
npm start
```

## .Env File

It is important to have the following environment variables configured.

```bash
# .env file

# SERVER #
PORT=4000

# DB #
DB_USER=root # or your user mongodb
DB_PASSWORD=password # or your password mongodb
DB_NAME=moviesdb
CONNECTION_STRING=localhost:27017
```

#

## Docker

Lift mongodb database with docker

```bash
docker-compose up -d
```

#

## API

### First Endpoint

```javascript
/* search */
/search?t={any word}
/search?t={any word}&page=1

/* get all records */
/search?all=true&page=1
```

### Second Endpoint

```javascript
/* search */
/update

/* body request */
{
  "movie": "Spiderman", // Title of the movie
  "find": "Jedi", // word to search for to replace
  "replace": "CML Dev" // word a to replace
}
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
