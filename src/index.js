const helmet = require("koa-helmet");
const cors = require("@koa/cors");
const koa = require("koa");
const dbConnection = require("./config/mongodb");
const {
  server: { PORT },
  logs: { path },
} = require("./config/config");
const bodyPaser = require("koa-bodyparser");
const respond = require("koa-respond");
const router = require("./routes");
const static = require("koa-static");

const app = new koa();

/* Setup database */
dbConnection();

/* Middlewares for use server */
app.use(helmet());
app.use(cors());
app.use(respond());
app.use(bodyPaser());
app.use(router());
app.use(static(path));

/* Server init */
app.listen(PORT, () => {
  console.log(`Koa server running on port ${PORT}`);
});
