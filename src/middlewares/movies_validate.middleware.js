const logger = require("../helpers/logger");

const validateRequest = (schema, type) => async (ctx, next) => {
  let { t, all } = ctx.request.query;
  try {
    if (type === 0) {
      await schema.validateAsync(ctx.request.query);

      if ((!all || !JSON.parse(all)) && !t)
        throw new Error("You must send the parameter /t/");
    } else await schema.validateAsync(ctx.request.body);

    await next();
  } catch (err) {
    logger(err, "validateRequest", "movies_validate.middleware.js");
    await ctx.send(400, err.message);
  }
};

module.exports = validateRequest;
