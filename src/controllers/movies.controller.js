const Movie = require("../models/Movie");
const {
  pagination: { LIMIT },
} = require("../config/config");
const logger = require("../helpers/logger");

const search = async (ctx) => {
  let { t, y, page, all } = ctx.request.query;
  if (!page) page = 1;
  const dataToSearch = all ? {} : { title: new RegExp(t, "i"), year: y };

  if (!y) delete dataToSearch.year;

  try {
    let movies = await Movie.find(dataToSearch)
      .skip((page - 1) * LIMIT)
      .limit(LIMIT);
    const totalResults = await Movie.count();
    const totalPages = Math.ceil(totalResults / LIMIT);

    return ctx.send(200, {
      search: movies,
      totalResults,
      totalPages,
    });
  } catch (err) {
    logger(err, "search", "movies.controllers.js");
    return ctx.send(500, "An error occurred while searching for the title");
  }
};

const updateMovie = async (ctx) => {
  const { movie, find, replace } = ctx.request.body;

  try {
    const movieToFind = await Movie.findOne({ title: movie });

    if (!movieToFind) {
      logger("Movie not found", "updateMovie", "movies.controllers.js");
      return ctx.send(404, "Movie not found");
    }

    if (!movieToFind.plot.includes(find)) {
      logger("Movie not found", "updateMovie", "movies.controllers.js");
      return ctx.send(404, {
        response: false,
        message: "The word you wish to replace does not exist in the plot",
      });
    }

    const movieUpdated = await Movie.findByIdAndUpdate(
      movieToFind,
      { $set: { plot: movieToFind.plot.replace(find, replace) } },
      { new: true }
    );

    ctx.send(200, { movie: movieUpdated });
  } catch (err) {
    logger(err, "updateMovie", "movies.controllers.js");
    return ctx.send(500, "An error occurred while searching for the title");
  }
};

module.exports = { search, updateMovie };
