const Router = require("@koa/router");
const { search, updateMovie } = require("../controllers/movies.controller");
const validateRequest = require("../middlewares/movies_validate.middleware");
const {
  searchQuerySchema,
  updateBodySchema,
} = require("../schemas/movies.schemas");

const router = new Router();

router
  .get("/search", validateRequest(searchQuerySchema, 0), search)
  .post("/update", validateRequest(updateBodySchema, 1), updateMovie);

module.exports = router;
