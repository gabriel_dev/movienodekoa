const combineRouters = require("koa-combine-routers");
const moviesRouter = require("./movies.routes");

const router = combineRouters(moviesRouter);

module.exports = router;
