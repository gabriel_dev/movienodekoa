const Joi = require("joi");

const searchQuerySchema = Joi.object({
  t: Joi.string(),
  y: Joi.string(),
  page: Joi.string(),
  all: Joi.boolean(),
});

const updateBodySchema = Joi.object({
  movie: Joi.string().required(),
  find: Joi.string().required(),
  replace: Joi.string().required(),
});

module.exports = { searchQuerySchema, updateBodySchema };
