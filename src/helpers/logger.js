const fs = require("fs");
const {
  logs: { path },
} = require("../config/config");

const logger = (message, method, file, isServer) => {
  const date = new Date();
  const filename = `./public/logs/${date.getFullYear()}${
    date.getMonth() + 1
  }${date.getDate()}.log`;
  const msgError = !isServer
    ? `${date.getHours()}${date.getMinutes()}${date.getSeconds()} Error: ${message} || Method: ${method} || File: ${file}\n`
    : `\n=============== Error in server ===============\n${message}\nMethod: ${method} || File: ${file}\n`;

  if (!fs.existsSync(path)) {
    fs.mkdirSync(path, { recursive: true });
  }

  fs.appendFile(filename, msgError, (err) => {
    if (err) throw err;
  });
};

module.exports = logger;
