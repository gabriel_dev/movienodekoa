const { Schema, model } = require("mongoose");
const RatingSchema = require("./Rating");

const MovieSchema = Schema(
  {
    title: {
      type: String,
      require: [true, "The title is required"],
    },
    year: {
      type: Number,
      require: [true, "The year is required"],
    },
    released: {
      type: Date,
      require: [true, "The released is required"],
    },
    genre: {
      type: String,
      trim: true,
      require: [true, "The genre is required"],
    },
    director: {
      type: String,
      require: [true, "The director is required"],
    },
    actors: {
      type: String,
      require: [true, "At least one actor is required"],
    },
    plot: {
      type: String,
      require: [true, "The plot is required"],
    },
    ratings: {
      type: [RatingSchema],
      require: [true, "The ratings is required"],
    },
  },
  { timestamps: true }
);

module.exports = model("Movie", MovieSchema, "movies");
