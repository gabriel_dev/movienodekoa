const { Schema } = require("mongoose");

const RatingSchema = Schema({
  source: {
    type: String,
  },
  value: {
    type: String,
  },
});

module.exports = RatingSchema;
