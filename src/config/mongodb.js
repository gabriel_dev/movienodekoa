const mongoose = require("mongoose");
const {
  db: { CONNECTION_STRING },
} = require("../config/config");
const logger = require("../helpers/logger");

const dbConnection = async () => {
  try {
    await mongoose.connect(CONNECTION_STRING);
    console.log("Db Online");
  } catch (err) {
    logger(err, "dbConnection", "mongodb.js");
    throw new Error("Error to connect to Mongoose");
  }
};

module.exports = dbConnection;
