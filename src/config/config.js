require("dotenv").config();

module.exports = {
  server: {
    PORT: process.env.PORT,
  },
  db: {
    CONNECTION_STRING: `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.CONNECTION_STRING}/${process.env.DB_NAME}?authSource=admin`,
  },
  pagination: {
    LIMIT: 5,
  },
  logs: {
    path: "./public/logs",
  },
};
